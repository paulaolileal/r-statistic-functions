library(R6)

DistribuicaoExponencial <- R6Class("DistribuicaoExponencial",
  public = list(
    parametro = NULL,
    initialize = function(parametro = NULL) {
      self$parametro <- parametro
    },
	media = function() { 1 / self$parametro },
	variancia = function() { 1 / (self$parametro ^ 2) },
	desvioPadrao = function() { sqrt(self$variancia()) },
    calcular = function(x) {
		(exp(1) ^ (-1 * self$parametro * x))
	},
	calcularIntervalo = function(inicial, final) {
		self$calcular(inicial) - self$calcular(final)
	}
  )
)